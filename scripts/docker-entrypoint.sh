#!/bin/sh

sed -i "s/{BACKEND_HOST}/$BACKEND_HOST/" /etc/nginx/nginx.conf
sed -i "s/{BACKEND_PORT}/$BACKEND_PORT/" /etc/nginx/nginx.conf
sed -i "s/{CLIENT_MAX_BODY_SIZE}/$CLIENT_MAX_BODY_SIZE/" /etc/nginx/nginx.conf

exec /usr/sbin/nginx

user www-data;
worker_processes auto;
pid /run/nginx.pid;
daemon off;

events {
    worker_connections 1024;
    # multi_accept on;
}

http {
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    server_tokens off;

    proxy_buffering off;

    # server_names_hash_bucket_size 64;
    # server_name_in_redirect off;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
    ssl_prefer_server_ciphers on;

    ##
    # Logging Settings
    ##
    log_format custom '"$request" $status "$http_referer" "$sent_http_location"';
    access_log /dev/stdout custom;
    error_log /dev/stderr info;

    ##
    # Caching setting
    ##
    proxy_no_cache 1;
    proxy_cache_bypass 1;

    ##
    # Gzip Settings
    ##
    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_min_length 256;
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;

    server {
        listen 80;
        listen [::]:80 default ipv6only=on;

        # Make site accessible from http://localhost/
        server_name _;

        root /var/www/html/public;
        index index.php;

        add_header X-Frame-Options "SAMEORIGIN";
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Content-Type-Options "nosniff";
        charset utf-8;

        client_max_body_size {CLIENT_MAX_BODY_SIZE};

        location / {
            try_files $uri /index.php?$query_string;
        }

        location ~ .well-known/ {
            allow all;
        }

        location ~ (^|/)\. {
            deny all;
        }

        location ~* \.log$ {
            deny all;
        }

        location ~* sitemap.xml$ {
            try_files $uri /index.php?$query_string;
        }

        location ~* \.(?:js|css|png|jpe?g|gif|ico|pdf|html?|eot|svg|woff2?|ttf|xml)$ {
            try_files $uri /index.php?$query_string;

            expires max;
            log_not_found off;
            access_log off;
        }

        location /health-check {
            access_log off;
            keepalive_timeout 0;

            add_header Content-Type text/plain;
            return 200 'OK';
        }

        location ~ \.php$ {
            fastcgi_buffers 16 16k;
            fastcgi_buffer_size 32k;

            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass {BACKEND_HOST}:{BACKEND_PORT};
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }
    }
}

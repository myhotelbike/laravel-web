FROM nginx:1-alpine

ARG PUID=1000

ENV BACKEND_HOST="app"\
    BACKEND_PORT="9000"\
    CLIENT_MAX_BODY_SIZE="20M"

RUN mkdir -p /var/www/html \
    && adduser -u $PUID -D -g '' -h /var/www/html web \
    && adduser -S -G www-data www-data \
    && adduser web www-data \
    && chown web:web /var/www/html

ADD conf/nginx.conf /etc/nginx/nginx.conf
ADD scripts/* /usr/bin/

WORKDIR /var/www/html

CMD ["/usr/bin/docker-entrypoint.sh"]
EXPOSE 80
